class FixedCost < Deal
  def profit
    cost_without_discount - (cost_without_discount * discount_percent/100 + pay)
  end
end
