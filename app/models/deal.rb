class Deal < ActiveRecord::Base
  has_many :codepacks
  has_many :coupons, through: :codepacks
  
  # Актуальные по времени

  scope :actually_by_time, lambda { where("start_date < ? and DATETIME(start_date, '+' || life_time || ' hours') > ?", Time.now, Time.now) }
  scope :for_show, lambda { actually_by_time.where(place: false) }
  
  def profit
    raise 'Abstract method'
  end
  
  # кол-во забронированых кодов(купонов)
  def used_codes
    codepacks.where(used: true).size
  end
  
  #проверка валидности акции по времени
  def actually_by_time?
    (start_date.to_datetime < Time.now) && ((start_date.to_datetime + life_time.hours) > Time.now)
  end
  
  #проверка валидности акции по кол-ву клиентов
  def actually_by_clients?
    max_clients > used_codes
  end
  
  #проверка валидности акции по времени и кол-ву клиентов
  def actually?
    actually_by_time? && actually_by_clients?
  end
  
  # создание купона.
  def get_coupon
    code = codepacks.not_used_first
    unless code.blank? && actually?
      Coupon.create(codepack_id: code.id, user_id: 1)
      code.update_attribute(:used, true)
      self.reload
      self.update_attribute(:place, true) if used_codes == max_clients
    else
      false
    end
  end
  
end
