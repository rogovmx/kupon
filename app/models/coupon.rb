class Coupon < ActiveRecord::Base
  
  validates :user_id, :codepack_id, presence: true
  validate :deal_actually_by_time_and_clients
  
  belongs_to :codepack
#  delegate :deal, :to => :codepack, :allow_nil => true
  has_one :deal, through: :codepack
  
  #акция для купона состоялась?
  def deal_place?
    self.deal.place
  end
  
  
private
  #проверка валидности акции по времени и кол-ву клиентов
  def deal_actually_by_time_and_clients
    self.deal.actually?
  end
  
end
