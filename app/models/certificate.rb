class Certificate < Deal
  
  def profit
    cost_without_discount - pay
  end
end
