class Codepack < ActiveRecord::Base

  has_one :coupon
  belongs_to :deal
  #неиспользованые коды
  scope :not_used, lambda { where("used != ? OR used IS NULL", true) }
  #первый неиспользованый код
  scope :not_used_first, lambda { not_used.limit(1).first }
  
end
