json.array!(@deals) do |deal|
  json.extract! deal, :id, :name, :max_clients, :min_clients, :start_date, :life_time, :actually
  json.url deal_url(deal, format: :json)
end
