# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150425102631) do

  create_table "codepacks", force: :cascade do |t|
    t.integer  "deal_id"
    t.string   "code"
    t.boolean  "used",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "coupons", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "codepack_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "deals", force: :cascade do |t|
    t.string   "type"
    t.string   "name"
    t.integer  "max_clients"
    t.integer  "min_clients",           default: 1
    t.datetime "start_date"
    t.integer  "life_time"
    t.boolean  "place"
    t.integer  "pay",                   default: 0
    t.integer  "discount_percent",      default: 0
    t.integer  "cost_without_discount", default: 0
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

end
