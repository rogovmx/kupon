class CreateDeals < ActiveRecord::Migration
  def change
    create_table :deals do |t|
      t.string :type
      t.string :name
      t.integer :max_clients
      t.integer :min_clients, :default => 1
      t.datetime :start_date
      t.integer :life_time
      t.boolean :place #состоялась?
      t.integer :pay, :default => 0 #нужно заплатить
      t.integer :discount_percent, :default => 0 #процент скидки
      t.integer :cost_without_discount, :default => 0 # стоимость без акции

      t.timestamps null: false
    end
  end
end
