class CreateCodepacks < ActiveRecord::Migration
  def change
    create_table :codepacks do |t|
      t.integer :deal_id
      t.string :code
      t.boolean :used, :default => false
      t.timestamps null: false
    end
  end
end
